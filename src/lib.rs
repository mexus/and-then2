//! [![pipeline status](https://gitlab.com/mexus/and-then2/badges/master/pipeline.svg)](https://gitlab.com/mexus/and-then2/commits/master)
//! [![crates.io](https://img.shields.io/crates/v/and-then2.svg)](https://crates.io/crates/and-then2)
//! [![docs.rs](https://docs.rs/and-then2/badge.svg)](https://docs.rs/and-then2)
//!
//! [[Master docs]](https://mexus.gitlab.io/and-then2/and_then2/), [[Release docs]](https://docs.rs/and-then2/)
//!
//! # And Then II
//!
//! An alternative to a traditional `Future::and_then` combinator.
//!
//! This is an experimental crate! Be warned :)
//!
//! More info is coming...
//! ## License
//!
//! Licensed under either of
//!
//!  * Apache License, Version 2.0 ([LICENSE-APACHE](LICENSE-APACHE) or http://www.apache.org/licenses/LICENSE-2.0)
//!  * MIT license ([LICENSE-MIT](LICENSE-MIT) or http://opensource.org/licenses/MIT)
//!
//! at your option.
//!
//! ### Contribution
//!
//! Unless you explicitly state otherwise, any contribution intentionally submitted
//! for inclusion in the work by you, as defined in the Apache-2.0 license, shall be dual licensed as above, without any
//! additional terms or conditions.

extern crate futures;

use futures::{Async, Future, IntoFuture, Poll};
use std::mem;

enum StateMachine<Original, Resulting, F> {
    First(F, Original),
    Second(Resulting),
    EmptyState,
}

/// AndThen-like futures combinator. See [`FutureExt`](trait.FutureExt.html) for details.
pub struct AndThen2<Original, Resulting, F>(StateMachine<Original, Resulting, F>);

impl<Original, Intermediate, F> Future for StateMachine<Original, Intermediate::Future, F>
where
    Original: Future,
    Intermediate: IntoFuture,
    F: FnOnce(Original::Item) -> Intermediate,
{
    type Item = Result<Intermediate::Item, Intermediate::Error>;
    type Error = Original::Error;

    fn poll(&mut self) -> Poll<Self::Item, Self::Error> {
        loop {
            let previous_state = mem::replace(self, StateMachine::EmptyState);
            let new_state = match previous_state {
                StateMachine::First(f, mut original) => match original.poll()? {
                    Async::Ready(x) => StateMachine::Second(f(x).into_future()),
                    Async::NotReady => {
                        mem::replace(self, StateMachine::First(f, original));
                        return Ok(Async::NotReady);
                    }
                },
                StateMachine::Second(mut resulting) => match resulting.poll() {
                    Err(e) => return Ok(Async::Ready(Err(e))),
                    Ok(Async::Ready(res)) => return Ok(Async::Ready(Ok(res))),
                    Ok(Async::NotReady) => {
                        mem::replace(self, StateMachine::Second(resulting));
                        return Ok(Async::NotReady);
                    }
                },
                StateMachine::EmptyState => panic!("poll() called after completion"),
            };
            mem::replace(self, new_state);
        }
    }
}

impl<Original, Intermediate, F> Future for AndThen2<Original, Intermediate::Future, F>
where
    Original: Future,
    Intermediate: IntoFuture,
    F: FnOnce(Original::Item) -> Intermediate,
{
    type Item = Result<Intermediate::Item, Intermediate::Error>;
    type Error = Original::Error;

    fn poll(&mut self) -> Poll<Self::Item, Self::Error> {
        self.0.poll()
    }
}

/// An extension trait that provides a convenient method `and_then2`.
pub trait FutureExt: Future {
    /// Creates a future object that resolves into an item that contains a `Result` of a
    /// computation of the provided closure `f`.
    ///
    /// It is very similar to a traditional `Future::and_then`, but differs in an item's type.
    ///
    /// Might come in handy when you have futures with different error types and you don't want to
    /// collect them into an enum.
    fn and_then2<F, B>(self, f: F) -> AndThen2<Self, B::Future, F>
    where
        B: IntoFuture,
        F: FnOnce(Self::Item) -> B,
        Self: Sized,
    {
        AndThen2(StateMachine::First(f, self))
    }
}

impl<T: Future> FutureExt for T {}

#[cfg(test)]
mod tests {
    use super::*;

    #[derive(Debug, PartialEq)]
    struct Err1;
    #[derive(Debug, PartialEq)]
    struct Err2;

    fn plus_one_ok(x: usize) -> impl IntoFuture<Item = usize, Error = Err2> {
        futures::future::ok(x + 1)
    }

    fn just_err(_: usize) -> impl IntoFuture<Item = usize, Error = Err2> {
        futures::future::err(Err2)
    }

    #[test]
    fn test_ok_ok() {
        let f1 = futures::future::ok::<_, Err1>(1);
        let combined = f1.and_then2(plus_one_ok);
        assert_eq!(Ok(Ok(2)), combined.wait());
    }

    #[test]
    fn test_ok_err() {
        let f1 = futures::future::ok::<_, Err1>(1);
        let combined = f1.and_then2(just_err);
        assert_eq!(Ok(Err(Err2)), combined.wait());
    }

    #[test]
    fn test_err_ok() {
        let f1 = futures::future::err(Err1);
        let combined = f1.and_then2(plus_one_ok);
        assert_eq!(Err(Err1), combined.wait());
    }

    #[test]
    fn test_err_err() {
        let f1 = futures::future::err(Err1);
        let combined = f1.and_then2(just_err);
        assert_eq!(Err(Err1), combined.wait());
    }
}
