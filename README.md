# and-then2

[![pipeline status](https://gitlab.com/mexus/and-then2/badges/master/pipeline.svg)](https://gitlab.com/mexus/and-then2/commits/master)
[![crates.io](https://img.shields.io/crates/v/and-then2.svg)](https://crates.io/crates/and-then2)
[![docs.rs](https://docs.rs/and-then2/badge.svg)](https://docs.rs/and-then2)

[[Master docs]](https://mexus.gitlab.io/and-then2/and_then2/), [[Release docs]](https://docs.rs/and-then2/)

## And Then II

An alternative to a traditional `Future::and_then` combinator.

This is an experimental crate! Be warned :)

More info is coming...
### License

Licensed under either of

 * Apache License, Version 2.0 ([LICENSE-APACHE](LICENSE-APACHE) or http://www.apache.org/licenses/LICENSE-2.0)
 * MIT license ([LICENSE-MIT](LICENSE-MIT) or http://opensource.org/licenses/MIT)

at your option.

#### Contribution

Unless you explicitly state otherwise, any contribution intentionally submitted
for inclusion in the work by you, as defined in the Apache-2.0 license, shall be dual licensed as above, without any
additional terms or conditions.

License: MIT/Apache-2.0
